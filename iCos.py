#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Creado por Gerardo Mena
Listado de modem confirmados que son compatibles
 ZTE MF626
 ZTE MF110
 Huawei E160
 Huawei E173
 Huawei E153
"""
import time, glob, logging, Queue, urllib2,json
import signal, sys, serial, threading, binascii, re, random
from ConfigParser import SafeConfigParser
from gsmmodem.modem import GsmModem
import gsmmodem.exceptions

#End point del servicio que asigna y controla las recargas
END_URI = "http://127.0.0.1/private/clubs/request_services.php"
# listado de los modems que estan corriendo
bg_threads = []
#logging.basicConfig(format='%(levelname)s: %(message)s', level=logging.DEBUG)


def on_exit(sig, func=None):
    sys.stdout.write("\n")
    sys.stdout.flush()
    for red in bg_threads:
        red.stop()
    sys.exit(1)


def load_data_modem(file_name='config.conf'):
    parser = SafeConfigParser()
    parser.read(file_name)
    items = dict()
    for section_name in parser.sections():
        items[section_name] = dict(parser.items(section_name))
    return items


def verify_serials_ports(serial_port):
    """
    Lista los puertos seriales
    :Regresa la lista de los puertos seriales disponibles
    """
    if sys.platform.startswith('win'):
        ports = ['COM' + str(i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # Esto es para excluir la terminal /dev/tty
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnviromentError('Unsupported plataform')
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    try:
        result.index(serial_port)
        return serial_port
    except ValueError:
        return None


signal.signal(signal.SIGTERM, on_exit)
signal.signal(signal.SIGINT, on_exit)


class ServiceModem(threading.Thread):
    log = logging.getLogger('iCos.ServiceModem')

    def __init__(self, modem_data):
        threading.Thread.__init__(self)
        self.alive = True
        self.modem_data = modem_data
        self.daemon = True
        self.queue = Queue.Queue()

    def run(self):
        # inicializando el modem
        modem = GsmModem(self.modem_data['port'], int(self.modem_data['baudrate']),
                         smsReceivedCallbackFunc=self.save_sms)
        try:
            modem.connect()
        except gsmmodem.exceptions.TimeoutException as e:
            pass
        try:
            modem.waitForNetworkCoverage(10)
        except gsmmodem.exceptions.TimeoutException as e:
            pass

        modem.smsTextMode = False
        modem.write('AT+CMGD=4') #Borrar todos los mensajes guardados
        try:
            modem.processStoredSms()
        except:
            pass

        conn = ServiceConn(END_URI, modem, self.modem_data)

        while self.alive:
            try:
                rqst = {"type": "request","id_carrier": self.modem_data['id_carrier']}
                conn.revisar_solicitudes(rqst)
                conn.retornar_logs(self.queue)
                time.sleep(5)
                #{"type":"response",data":{"1":["enable","hola mundo"],"2":["disable","mas datos"]}}
                #{"type":"log","id_carrier":"3","contenido":"Hoy ha ganado algo"}
            except Exception as e:
                sys.stderr.write("Error fatal %s" % e.message)
                #print e
        sys.stdout.write("Cerrando puerto serial %s del operador %s\n" %(self.modem_data['port'],self.modem_data['operador']))
        modem.close()
        self.alive = False

    def stop(self):
        if self.alive:
            self.alive = False
            self.join()

    def save_sms(self, sms):
        if self.queue:
            self.queue.put(sms.number + ":" + sms.text)
        sys.stdout.write("Recibiendo SMS de la conexion con el operador %s\n" %(self.modem_data['operador']))
        self.log.info(u'Mensaje recibido de \n{0}\nHora:{1}\nMensaje:{2}'.format(sms.number, sms.time, sms.text))


class ModemCommand(object):
    log = logging.getLogger('iCos.ModemCommand')

    def __init__(self,modem, modem_data):
        self.modem = modem
        self.manufacturer = modem.manufacturer
        self.model = modem.model
        self.modem_data = modem_data

    def send_ussd(self,ussd_string):
        response = ""
        if self.manufacturer.lower() == 'huawei' and self.model.lower() == 'e160':
            ussd_string = self.encode(ussd_string)
        elif self.manufacturer.lower() == 'huawei' and self.model.lower() == 'e153':
            ussd_string = self.encode(ussd_string)
        print (ussd_string)

        ussd_response = self.modem.sendUssd(ussd_string)

        if ussd_response.sessionActive:
            ussd_response.cancel()
        response = self._format_response(ussd_response)
        return response

    def send_sms(self,short_code, sms_string=""):
        try:
            self.modem.sendSms(short_code, sms_string, True, 3)
        except gsmmodem.exceptions.TimeoutException as e:
            # self.log.error('Error enviando sms al shortcode %s',short_code)
            return True
        except Exception as e:
            return False
        return True

    def _format_response(self, ussd_response):

        control_chars = ''.join(map(unichr, range(0,32) + range(127,160)))

        control_char_re = re.compile('[%s]' % re.escape(control_chars))

        if self.manufacturer.lower() == 'huawei' and self.model.lower()=='e160':
            response_message = self.decode(ussd_response.message)
        elif self.manufacturer.lower() == 'zte incorporated' and self.model.lower()=='mf110':
            response_message = binascii.unhexlify(ussd_response.message).decode('utf-16-be')
        elif self.manufacturer.lower() == 'huawei' and self.model.lower()=='e153':
            response_message = self.decode(ussd_response.message)
        else:
            response_message = ussd_response.message

        return control_char_re.sub(' ', response_message) #Remover carateres no imprimibles

    def encode(self,text, padding=0):
        return self._pack_septets(text, padding).encode('hex').upper()

    def decode(self,pdu, padding=0):
        return self._unpack_septets(pdu, padding)

    def _pack_septets(self,str, padding=0):
        bytes=[ord(c) for c in str]
        bytes.reverse()
        asbinary = ''.join([self._to_binary(b)[1:] for b in bytes])
        # add padding
        for i in range(padding):
            asbinary+='0'

        # zero extend last octet if needed
        extra = len(asbinary) % 8
        if extra>0:
            for i in range(8-extra):
                asbinary='0'+asbinary

        # convert back to bytes
        bytes=[]
        for i in range(0,len(asbinary),8):
            bytes.append(int(asbinary[i:i+8],2))
        bytes.reverse()
        return ''.join([chr(b) for b in bytes])

    def _unpack_septets(self,seq,padding=0):
        """
        this function taken from:
        http://offog.org/darcs/misccode/desms.py

        Thank you Adam Sampson <ats@offog.org>!
        """

        # Unpack 7-bit characters
        msgbytes,r = self._consume_bytes(seq,len(seq)/2)
        msgbytes.reverse()
        asbinary = ''.join(map(self._to_binary, msgbytes))
        if padding != 0:
            asbinary = asbinary[:-padding]
        chars = []
        while len(asbinary) >= 7:
            chars.append(int(asbinary[-7:], 2))
            asbinary = asbinary[:-7]
        return "".join(map(chr, chars))

    def _consume_bytes(self,seq, num=1):
        """
        consumes bytes for num ints (e.g. 2-chars per byte)
        coverts to int, returns tuple of  ([byte...], remainder)
        """
        bytes=[]
        for i in range(0, self._B(num), 2):
            bytes.append(int(seq[i:i+2],16))

        return (bytes,seq[self._B(num):])

    def _B(self,slot):
        """Convert slot to Byte boundary"""
        return slot * 2

    def _to_binary(self, n):
        s = ''
        for i in range(8):
            s = ('%1d' % (n & 1)) + s
            n >>= 1
        return s


class ServiceConn(object):
    MAX_CONN_ATTEMPT = 10
    TIME_OUT = 10
    log = logging.getLogger('iCos.ServiceConn')

    def __init__(self, uri_service, modem, modem_data):
        self.modem = modem
        self.uri = uri_service
        self.queue = None
        self.modem_command = ModemCommand(modem, modem_data)
        self.modem_data = modem_data
        #self.stop = threading.Event()

    def revisar_solicitudes(self, data):
        sc = self.modem_data['sms_sc']
        self.log.info("%s:Realizando solicitud %s" %(self.modem_data['operador'], data))
        response_site = self._connect_to_service(data=data)
        response_dict = {} #Diccionario que guarda las respuestas
        if response_site is not None:
            if response_site['status'] == 0:
                #Recorremos el arreglo
                for id_data in response_site['data']:
                    #Revisamos si es ussd o sms
                    str_req = self._format_request_modem(response_site['data'][id_data])
                    if response_site['data'][id_data][0] == 'ussd' and str_req:
                        self.log.info("%s:Solicitud de comando ussd %s" %(self.modem_data['operador'], str_req))
                        sys.stdout.write("%s:Realizando solicitud ussd:[%s] \n" % (self.modem_data['operador'], str_req))
                        response = self.modem_command.send_ussd(str_req)
                        self.log.info("%s:Respuesta de comando ussd %s" %(self.modem_data['operador'], response))
                        if self._check_responses_modem('ussd',response):
                            sys.stdout.write("%s:Asignacion ussd:[%s] exitosa \n" % (self.modem_data['operador'], str_req))
                            status = 'enable'
                        else:
                            sys.stderr.write("%s:Respuesta ussd no esperada:[%s]\n" % (self.modem_data['operador'], response))
                            status = 'disable'
                        #self.retornar_respuesta({id_data:[status, response]})
                    elif response_site['data'][id_data][0] == 'sms' and str_req:
                        sys.stderr.write("%s:Realizando solicitud sms:[%s] \n" % (self.modem_data['operador'],str_req))
                        if self.modem_command.send_sms(sc, str_req):
                            sys.stdout.write("%s:Envio de sms:[%s] exitoso \n" % (self.modem_data['operador'], str_req))
                            response = "sms enviado"
                            status = 'enable'
                        else:
                            sys.stderr.write("%s:El envio de sms no fue posible\n" % (self.modem_data['operador']))
                            response ="Error desconocido"
                            status = 'disable'
                        #self.retornar_respuesta({id_data:['enable', "sms enviado"]})
                    else:
                        pass
                    response_dict[id_data] = [status, response] #Lo agregamos al arreglo
                #Aqui enviamos todas las respuestas
                if len(response_site['data']):
                    self.log.info("%s:Resultado de operaciones %s" %(self.modem_data['operador'], json.dumps(response_dict)))
                    self.retornar_respuestas(response_dict)
                else:
                    self.log.info("%s:No hay datos que procesar para el carrier id:%s" %(self.modem_data['operador'], self.modem_data['id_carrier']))
                    sys.stdout.write("%s:No hay solicites que procesar\n" % (self.modem_data['operador']))
            else:
                return False
        else:
            return False

    def retornar_respuestas(self, data):
        rqst = {"type": "response", "data": data}
        sys.stdout.write("%s:Retornando respuesta de las solicitudes [%s]\n" % (self.modem_data['operador'], json.dumps(rqst)))
        response_site = self._connect_to_service(data=rqst)
        if response_site is not None:
            if response_site['status'] == 0:
                return True
            else:
                sys.stderr.write("%s:Respuesta del servicio incorrecta a entregar resultados\n" % (self.modem_data['operador']))
                return False
        else:
            return False

    def retornar_logs(self, queue):
        while not queue.empty():
            sms_text = queue.get()
            queue.task_done()
            #{"type":"log","id_carrier":"3","contenido":"Hoy ha ganado algo"}
            rqst = {"type": "log", "id_carrier": self.modem_data['id_carrier'],"contenido": sms_text}
            sys.stdout.write("%s:Guardando los sms recibidos [%s]\n" % (self.modem_data['operador'], json.dumps(rqst)))
            response_site = self._connect_to_service(data=rqst, retry=1)

    def _connect_to_service(self, data, retry=None):
        if retry is None:
            retry = self.MAX_CONN_ATTEMPT
        req = urllib2.Request(self.uri,json.dumps(data),{'Content-Type': 'application/json'})
        for i in range(retry):
            try:
                sys.stdout.write("%s:Conectando al url %s\n" %(self.modem_data['operador'], self.uri))
                site = urllib2.urlopen(req,timeout=self.TIME_OUT)
            except urllib2.HTTPError, urllib2.URLError:
                sys.stderr.write("%s:Servicio %s no disponible, reintentando conexion nuevamente...\n" %(self.modem_data['operador'],self.uri))
                site_response = None
                #self.stop.wait((2 ** i) + (random.randint(0, 1000) / 1000))
                time.sleep((2 ** i) + (random.randint(0, 1000) / 1000))
            else:
                site_response = site.read()
                sys.stdout.write("%s:Conexion exitosa\n" % (self.modem_data['operador']))
                self.log.info("%s:Respuesta del servicio [%s]" % (self.modem_data['operador'], json.loads(site_response)))
                break
        if site_response is not None:
            return json.loads(site_response)
        else:
            return None

    def _format_request_modem(self, data_array):
        ussd_str = self.modem_data['ussd_string']
        sms_str = self.modem_data['sms_string']
        ussd_str = ussd_str.replace('/telefono/', data_array[1])
        ussd_str = ussd_str.replace('/cantidad/', str(data_array[2]))
        sms_str = sms_str.replace('/telefono/', data_array[1])
        sms_str = sms_str.replace('/cantidad/', str(data_array[2]))
        if data_array[0] == 'ussd':
            return ussd_str
        elif data_array[0] == 'sms':
            return sms_str
        else:
            return None

    def _check_responses_modem(self, tipo='ussd', response=''):
        ussd_response = self.modem_data['ussd_ok']
        list_ok = ussd_response.split('|')
        sms_response = self.modem_data['sms_ok']
        list_ok_sms = sms_response.split('|')

        if tipo == 'ussd':
            return self._check_list(list_ok, response)
        else:
            return self._check_list(list_ok_sms, response)

    def _check_list(self, lista, response=''):
        success = False
        for item in lista:
            OK = re.compile(r'%s' % item)
            match = OK.match(response)
            if match:
                success = True
                break
            else:
                success = False
        return success

if __name__ == '__main__':
    modems_data = load_data_modem()
    if not modems_data:
        sys.stdout("No existe información acerca de los modems\n")
        on_exit(signal.SIGTERM)
    for modem in modems_data:
        if verify_serials_ports(modems_data[modem]['port']) is not None:
            sys.stdout.write("Estableciendo conexión al puerto %s de la red %s\n" %(modems_data[modem]['port'],modems_data[modem]['operador']))
            service_modem = ServiceModem(modems_data[modem])
            service_modem.start()
            bg_threads.append(service_modem)
        else:
            sys.stderr.write("Puerto %s no disponible para la red %s\n" %(modems_data[modem]['port'],modems_data[modem]['operador']))

    while True:
        if not len(bg_threads):
            sys.stdout.write("Ningun modem detectado. Se requiere tener conexión a los puertos seriales")
            break
        input_data = raw_input("Presione 'Control-C' para cancelar\n")
    on_exit(signal.SIGTERM)